<?php

use App\Http\Controllers\Api\Catalog\BusinessTypeController;
use App\Http\Controllers\Api\Catalog\CategoryController;
use App\Http\Controllers\Api\Catalog\CountryController;
use App\Http\Controllers\Api\Catalog\ServiceOrderController;
use App\Http\Controllers\Api\Interactions\ChatController;
use App\Http\Controllers\Api\Interactions\MessageController;
use App\Http\Controllers\Api\User\AuthController;
use App\Http\Controllers\Api\User\OrderOfferController;
use App\Http\Controllers\Api\User\Profile\WorkController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\Users\FreelancerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('/categories', CategoryController::class);
Route::resource('/countries', CountryController::class);
Route::resource('/business-types', BusinessTypeController::class);
Route::resource('/freelancers',FreelancerController::class);

Route::prefix('/auth')->group(function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
});

Route::middleware('auth:sanctum')->group(function(){
    Route::prefix('/user')->group(function () {
        Route::get('/',[UserController::class,'index']);
        Route::post('/',[UserController::class,'update']);
    });

    Route::prefix('/service-orders')->group(function(){
        Route::post('/',[ServiceOrderController::class,'index']);
        Route::post('/store',[ServiceOrderController::class,'store']);
        Route::post('/offer/{offerOrder}',[ServiceOrderController::class,'order']);
        Route::post('/decline/{offerOrder}',[ServiceOrderController::class,'decline']);
        Route::post('/approve/{offerOrder}',[ServiceOrderController::class,'approve']);
        Route::post('/finish/{offerOrder}',[ServiceOrderController::class,'finish']);
    });

    Route::resource('/offer',OrderOfferController::class);


    Route::resource('/chat',ChatController::class);

    Route::resource('/message',MessageController::class);

    Route::resource('/work',WorkController::class);
});

