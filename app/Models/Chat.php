<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_order_id',
        'customer_id',
        'executor_id',
    ];

    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('id', 'desc');
    }

    public function executor()
    {
        return $this->belongsTo(User::class, 'executor_id', 'id');
    }

    public function opposite_user($id)
    {
        if ($this->cusomter_id == $id)
            return $this->executor();
        else
            return $this->customer();
    }

    public function service()
    {
        return $this->belongsTo(ServiceOrder::class);
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }
}
