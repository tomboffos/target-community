<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // User order requests

    public function offers()
    {
        return $this->hasMany(OrderOffer::class)->orderBy('id', 'desc');
    }


    public function works()
    {
        return $this->hasMany(Work::class);
    }


    public function chats()
    {
        if ($this->role_id == 2)
            return $this->hasMany(Message::class, 'executor_id', 'id');

        return $this->hasMany(Message::class, 'customer_id', 'id');
    }



    public function rating()
    {
        return $this->hasMany(UserReview::class)->sum('rating') / $this->hasMany(UserReview::class)->count();
    }


    public function device_tokens()
    {
        return $this->hasMany(UserDeviceToken::class)->pluck('device_token');
    }
}
