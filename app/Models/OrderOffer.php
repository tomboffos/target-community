<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderOffer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'description',
        'offer_budget',
        'price',
        'days',
        'currency_id',
        'service_order_id',
    ];

    public function customer()
    {
        return $this->hasOneThrough(User::class,ServiceOrder::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function service()
    {
        return $this->belongsTo(ServiceOrder::class);
    }
}
