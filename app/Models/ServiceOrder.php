<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'currency_id',
        'category_id',
        'budget',
        'price',
        'title',
        'description',
        'days',
        'status_id'
    ];


    public function status()
    {
        return $this->hasMany(Status::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function business_types()
    {
        return $this->belongsToMany(BusinessType::class, 'service_order_business_type');
    }

    public function cities()
    {
        return $this->belongsToMany(BusinessType::class, 'service_order_city');
    }
}
