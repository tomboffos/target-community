<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'order',
        'category_id',
        'image'
    ];

    public function children()
    {
        return $this->hasMany(Category::class);
    }

    public function parent()
    {
        return $this->hasMany(Category::class);
    }
}
