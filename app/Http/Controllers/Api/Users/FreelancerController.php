<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserType\FreelancerResource;
use App\Models\User;
use Illuminate\Http\Request;

class FreelancerController extends Controller
{
    //
    public function index(Request $request)
    {
        return FreelancerResource::collection(User::where('role_id',2)->inRandomOrder()->paginate(10));
    }

    public function show(User $id)
    {
        return new FreelancerResource($id);
    }
}
