<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\BusinessTypeResource;
use App\Models\BusinessType;
use Illuminate\Http\Request;

class BusinessTypeController extends Controller
{
    //
    public function index()
    {
        return BusinessTypeResource::collection(BusinessType::get());
    }

    public function show(BusinessType $id)
    {
        return BusinessTypeResource::collection($id->children);
    }
}
