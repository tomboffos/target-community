<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ServiceOrderStoreRequest;
use App\Http\Requests\ServiceOrderIndexRequest;
use App\Http\Resources\Api\ServiceOrderResource;
use App\Models\BusinessType;
use App\Models\City;
use App\Models\OrderOffer;
use App\Models\ServiceOrder;
use App\Services\ServiceOrderService;
use Illuminate\Http\Request;

class ServiceOrderController extends Controller
{
    //
    public function index(ServiceOrderIndexRequest $request)
    {
        return ServiceOrderResource::collection(ServiceOrder::where(function($query) use ($request){
            if ($request->has('business_types'))
                $query->whereHas('business_types',function($childRequest) use ($request){
                   $childRequest->whereIn('id',$request->business_types);
                });
            if ($request->has('cities'))
                $query->whereHas('cities',function($childRequest) use ($request){
                   $childRequest->whereIn('id',$request->cities);
                });
            if ($request->has('price_range'))
                $query->whereBetween('price',$request->price_range);

            if ($request->has('budget_range'))
                $query->whereBetween('budget',$request->budget_range);

            if ($request->has('service_id'))
                $query->where('service_id',$request->service);

            if ($request->has('search')){
                $search = strlen($request->search) > 3 ? '%'.$request->search.'%' : $request->search.'%';
                $query->where('name','iLIKE',$search);
            }

        })->whereIn('status_id',[1,2])->orderBy($request->sort_type,$request->sort_value)->paginate(10));
    }

    public function store(ServiceOrderStoreRequest $request)
    {
        $data = $request->except('cities','business_types');

        $data['user_id'] = $request->user()->id;
        $data['status_id'] = 1;

        $service = ServiceOrder::create($data);

        $service = new ServiceOrderService($service);

        $service->attachBindings([
            'cities' => $request->cities,
            'business_types' => $request->business_types
        ]);

        return new ServiceOrderResource($service);
    }


    public function order(OrderOffer $orderOffer)
    {
        $orderOffer->service->update([
            'status_id' => 2
        ]);
        $orderOffer->update([
            'offer_status_id' => 5
        ]);

        return response([
            'message' => 'Работа была предложена успешна дождитесь ответа исполнителя',
        ],200);
    }

    public function decline(OrderOffer $orderOffer)
    {
        $orderOffer->update([
           'offer_status_id' => 2
        ]);

        return response([
            'message' => 'Работа была успешна отклонена'
        ],200);
    }

    public function approve(OrderOffer $orderOffer,Request $request)
    {
        if ($request->user()->id == $orderOffer->user->id){
            $orderOffer->update([
                'offer_status_id' => 3
            ]);
            $orderOffer->service->update([
                'status_id' => 3
            ]);
            return response(['message' => 'Заявка в работе'],200);
        }

        return  response(['message' => 'Вы не авторизованы'],422);
    }

    public function finish(OrderOffer $orderOffer)
    {
        $orderOffer->update([
            'offer_status_id' => 4
        ]);

        $orderOffer->service()->update([
            'status_id' => 4
        ]);

        return response(['message' => 'Работа была выполнена']);
    }
}

