<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //
    public function index()
    {
        return CountryResource::collection(Country::get());
    }

    public function show(Country $id)
    {
        return CityResource::collection($id->cities);
    }

}
