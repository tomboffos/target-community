<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //

    public function login(LoginRequest $request)
    {
        $user = User::where('email',$request->email)->first();
        if (!$user)
            return response(['message' => 'Вы ввели не правильный логин или пароль'],404);

        if (Hash::check($request->password,$user['password']))
            return response([
                'user' => new UserResource($user),
                'token' =>  $user->createToken(env('APP_NAME'))->plainTextToken
            ]);


        return response(['message' => 'Вы ввели не правильный логин или пароль'],404);

    }


    public function register(RegisterRequest $request)
    {
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(env('APP_NAME'))->plainTextToken
        ],200);
    }

}
