<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\OrderOfferStoreRequest;
use App\Http\Resources\Api\User\OrderOfferResource;
use App\Models\OrderOffer;
use Illuminate\Http\Request;

class OrderOfferController extends Controller
{
    //

    public function index(Request $request)
    {
        return OrderOfferResource::collection($request->user()->offers);
    }

    public function store(OrderOfferStoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] =  $request->user();
        $data['offer_status_id'] = 1;
        $offer = OrderOffer::create($data);

        return new OrderOfferResource($offer);
    }
}
