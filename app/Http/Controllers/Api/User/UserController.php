<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    private $service;

    public function __construct()
    {
        $this->service = new UserService();
    }

    public function index(Request $request)
    {
        return new UserResource($request->user());
    }

    public function update(UserUpdateRequest $request)
    {
        $data = $request->validated();

        if ($data['password'])
            $data['password'] = Hash::make($data['password']);

        if ($data['avatar'])
            $data['avatar'] = $this->service->handleImage($data['avatar']);

        $request->user()->update($data);

        return new UserResource($request->user());
    }

}
