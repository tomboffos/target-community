<?php

namespace App\Http\Controllers\Api\User\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Profile\WorkStoreRequest;
use App\Http\Resources\Api\User\Profile\WorkResource;
use App\Models\Work;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    //
    public function index(Request $request)
    {
        return WorkResource::collection($request->user()->works);
    }

    public function store(WorkStoreRequest $request)
    {
        $data = $request->validated();

        $data['user_id'] = $request->user()->id;

        $work = Work::create($data);

        return new WorkResource($work);
    }
}
