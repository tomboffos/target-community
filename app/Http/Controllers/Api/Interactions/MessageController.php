<?php

namespace App\Http\Controllers\Api\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Interactions\Message\MessageStoreRequest;
use App\Http\Resources\Interactions\MessageResource;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    //
    public function store(MessageStoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user();

        $message = Message::create($data);

        return new MessageResource($message);
    }
}
