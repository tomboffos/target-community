<?php

namespace App\Http\Controllers\Api\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Interactions\Chat\ChatStoreRequest;
use App\Http\Resources\Interactions\ChatResource;
use App\Http\Resources\Interactions\MessageResource;
use App\Models\Chat;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    //

    public function index(Request $request)
    {
        return ChatResource::collection($request->user()->chats);
    }

    public function store(ChatStoreRequest $request)
    {
        $data = $request->validated();

        $chat = Chat::create($data);

        return new ChatResource($chat);
    }

    public function show(Chat $id)
    {
        return MessageResource::collection($id->messages);
    }
}
