<?php

namespace App\Http\Requests\Interactions\Chat;

use Illuminate\Foundation\Http\FormRequest;

class ChatStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'service_order_id' => 'required|exists:service_orders,id',
            'customer_id' => 'required|exists:users,id',
            'executor_id' => 'required|exists:users,id'
        ];
    }
}
