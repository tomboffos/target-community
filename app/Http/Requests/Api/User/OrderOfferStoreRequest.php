<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class OrderOfferStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'service_order_id' => 'required|exists:service_orders,id',
            'price' => 'required|numeric',
            'days' => 'required|numeric',
            'offered_budget' => 'nullable|numeric',
            'currency_id' => 'required|exists:currencies,id',
            'description' => 'nullable'
        ];
    }
}
