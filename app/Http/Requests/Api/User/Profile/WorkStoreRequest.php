<?php

namespace App\Http\Requests\Api\User\Profile;

use Illuminate\Foundation\Http\FormRequest;

class WorkStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'images' => 'required',
            'title' => 'required',
            'description' => 'required',
            'budget' => 'nullable|numeric',
            'price' => 'nullable|numeric',
            'days' => 'nullable|numeric'

        ];
    }
}
