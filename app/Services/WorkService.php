<?php


namespace App\Services;


class WorkService
{

    public function handleImages($data)
    {
        $images = [];

        foreach ($data['images'] as $image){
            $images[] = asset(str_replace('/public','',$image->store('/public/uploads/works/')));
        }

        $data['images'] = $images;

        return $data;
    }
}
