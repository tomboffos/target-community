<?php


namespace App\Services;


class UserService
{
    public function handleImage($image)
    {
        return asset(str_replace('/public','',$image->store('/public/uploads/works/')));
    }

}
