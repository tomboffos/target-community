<?php


namespace App\Services;


use App\Models\BusinessType;
use App\Models\City;
use App\Models\ServiceOrder;

class ServiceOrderService
{
    private $serviceOrder;

    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    public function attachCities($data)
    {
        foreach ($data as $city) {
            $city = City::find($city);
            $this->serviceOrder->cities()->attach($city);
        }
    }

    public function attachTypes($data)
    {
        foreach ($data as $item){
            $businessType = BusinessType::find($item);
            $this->serviceOrder->cities()->attach($businessType);
        }
    }

    public function attachBindings($data)
    {
        $this->attachBindings($data['cities']);
        $this->attachCities($data['business_types']);
    }
}
