<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class NotificationService
{

    private $serverKey = '';

    public function send(array $tokens, string $title, string $text)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json'
        ])->post('https://fcm.googleapis.com/fcm/send', [
            'registration_ids' => $tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                'sound' => 'default',
            ]
        ]);
        Log::info("Notification service : ".$response->body());
    }

}
