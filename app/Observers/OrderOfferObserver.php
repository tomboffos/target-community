<?php

namespace App\Observers;

use App\Models\OrderOffer;
use App\Services\NotificationService;
use Kreait\Laravel\Firebase\Facades\Firebase;

class OrderOfferObserver
{
    private $notify;

    public function __construct()
    {
        $this->notify = new NotificationService();
    }

    /**
     * Handle the OrderOffer "created" event.
     *
     * @param \App\Models\OrderOffer $orderOffer
     * @return void
     */
    public function created(OrderOffer $orderOffer)
    {
        //
        $this->notify->send($orderOffer->customer->device_tokens,
            'Обьявление '.$orderOffer->service->title,
            'На ваше ' . $orderOffer->service->title . 'обьявление ответил(-а) ' . $orderOffer->user->name);
    }

    /**
     * Handle the OrderOffer "updated" event.
     *
     * @param \App\Models\OrderOffer $orderOffer
     * @return void
     */
    public function updated(OrderOffer $orderOffer)
    {
        //
    }

    /**
     * Handle the OrderOffer "deleted" event.
     *
     * @param \App\Models\OrderOffer $orderOffer
     * @return void
     */
    public function deleted(OrderOffer $orderOffer)
    {
        //
    }

    /**
     * Handle the OrderOffer "restored" event.
     *
     * @param \App\Models\OrderOffer $orderOffer
     * @return void
     */
    public function restored(OrderOffer $orderOffer)
    {
        //
    }

    /**
     * Handle the OrderOffer "force deleted" event.
     *
     * @param \App\Models\OrderOffer $orderOffer
     * @return void
     */
    public function forceDeleted(OrderOffer $orderOffer)
    {
        //
    }
}
