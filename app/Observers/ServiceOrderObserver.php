<?php

namespace App\Observers;

use App\Models\ServiceOrder;

class ServiceOrderObserver
{
    /**
     * Handle the ServiceOrder "created" event.
     *
     * @param  \App\Models\ServiceOrder  $serviceOrder
     * @return void
     */
    public function created(ServiceOrder $serviceOrder)
    {
        //
    }

    /**
     * Handle the ServiceOrder "updated" event.
     *
     * @param  \App\Models\ServiceOrder  $serviceOrder
     * @return void
     */
    public function updated(ServiceOrder $serviceOrder)
    {
        //
    }

    /**
     * Handle the ServiceOrder "deleted" event.
     *
     * @param  \App\Models\ServiceOrder  $serviceOrder
     * @return void
     */
    public function deleted(ServiceOrder $serviceOrder)
    {
        //
    }

    /**
     * Handle the ServiceOrder "restored" event.
     *
     * @param  \App\Models\ServiceOrder  $serviceOrder
     * @return void
     */
    public function restored(ServiceOrder $serviceOrder)
    {
        //
    }

    /**
     * Handle the ServiceOrder "force deleted" event.
     *
     * @param  \App\Models\ServiceOrder  $serviceOrder
     * @return void
     */
    public function forceDeleted(ServiceOrder $serviceOrder)
    {
        //
    }
}
