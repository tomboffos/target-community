<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfferStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('offer_statuses')->insert([
            'name' => 'Заявка отправлена',
            'id' => 1
        ]);
        DB::table('offer_statuses')->insert([
            'name' => 'Заявка отклонена',
            'id' => 2
        ]);
        DB::table('offer_statuses')->insert([
            'name' => 'Заявка в работе',
            'id' => 3
        ]);
        DB::table('offer_statuses')->insert([
            'name' => 'Заявка была выполнена',
            'id' => 4
        ]);
        DB::table('offer_statuses')->insert([
            'name' => 'Предложена работа',
            'id' => 5
        ]);

    }
}
