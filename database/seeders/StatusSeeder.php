<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('statuses')->insert([
            'name' => 'Свободный заказ',
            'id' => 1
        ]);
        DB::table('statuses')->insert([
           'name' => 'Предложена работа',
           'id' => 2
        ]);
        DB::table('statuses')->insert([
            'name' => 'Заказ в работе',
            'id' => 3
        ]);
        DB::table('statuses')->insert([
            'name' => 'Заказ выполнен',
            'id' => 4
        ]);
    }
}
